var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return 'id: '+this.id+" | color: "+this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if(aBici){
        return aBici;
    }
    else{
        throw new Error(`No existe una bici con el Id ${aBiciId}`);
    }
}

Bicicleta.removeById= function (aBiciId){
    Bicicleta.findById(aBiciId);
    for(var i=0; i< Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}

var a = new Bicicleta(0, 'Rojo', 'Urbana', [4.7, -74.05]);
var b = new Bicicleta(1, 'Blanca', 'Urbana', [4.72, -74.06]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;